//*************
// SYM CIPHER FILE
//*************

// https://stackoverflow.com/questions/60520526/aes-encryption-and-decryption-of-files-using-crypto-js
// https://stackoverflow.com/questions/73551878/how-to-encrypt-files-with-aes-algorithm-with-cryptojs

(function() {

    "use strict";

    // encrypt
    function encrypt(key, input) {
        var file = input.files[0];
        var reader = new FileReader();
        reader.onload = () => {
            // convert: ArrayBuffer -> WordArray
            var wordArray = CryptoJS.lib.WordArray.create(reader.result);       
              
            const keyParsed = CryptoJS.enc.Hex.parse(key);

            // create random IV (Initialization Vector) - Reminder: 128 bits block (16 bytes) for 256 bits key
            const CRYPTO_IV_LENGTH = 16
            const iv = CryptoJS.lib.WordArray.random(CRYPTO_IV_LENGTH);

            // encrypt
            var encrypted = CryptoJS.AES.encrypt(wordArray, keyParsed, {
                keySize: 256, 
                iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            // prepare IV attachment to encryptedMessage to enable further decryption
            const cipherString = CryptoJS.enc.Base64.stringify(encrypted.ciphertext);
            
            // encrypted
            encrypted = `${iv.toString(CryptoJS.enc.Base64)}:${cipherString}`;

            // create blob from string
            var fileEnc = new Blob([encrypted]);                                    

            var a = document.createElement("a");
            var url = window.URL.createObjectURL(fileEnc);
            var filename = file.name + ".enc";
            a.href = url;
            a.download = filename;
            a.click();
            window.URL.revokeObjectURL(url);
        };
        reader.readAsArrayBuffer(file);
    }

    // decrypt
    function decrypt(key, input) {
        var file = input.files[0];
        var reader = new FileReader();
        reader.onload = () => {
            try {
                // split IV and encrypted message
                var encrypted = reader.result;
                const [ivBase64, cipher] = encrypted.split(':');
                const cipherParsed = CryptoJS.enc.Base64.parse(cipher); 
                const decrypted = CryptoJS.AES.decrypt({ciphertext: cipherParsed}, CryptoJS.enc.Hex.parse(key), {
                    keySize: 256, 
                    iv: CryptoJS.enc.Base64.parse(ivBase64),
                    mode: CryptoJS.mode.CBC
                });

                const typedArray = convertWordArrayToUint8Array(decrypted);

                // Create blob from typed array
                var fileDec = new Blob([typedArray]);                                   

                var a = document.createElement("a");
                var url = window.URL.createObjectURL(fileDec);
                var filename = file.name.substr(0, file.name.length - 4);
                a.href = url;
                a.download = filename;
                a.click();
                window.URL.revokeObjectURL(url);
            } 
            catch (error) {
                console.log('Erreur: ' + error);
            }
        };
        reader.readAsText(file);
    }

    function convertWordArrayToUint8Array(wordArray) {
        var arrayOfWords = wordArray.hasOwnProperty("words") ? wordArray.words : [];
        var length = wordArray.hasOwnProperty("sigBytes") ? wordArray.sigBytes : arrayOfWords.length * 4;
        var uInt8Array = new Uint8Array(length), index=0, word, i;
        for (i=0; i<length; i++) {
            word = arrayOfWords[i];
            uInt8Array[index++] = word >> 24;
            uInt8Array[index++] = (word >> 16) & 0xff;
            uInt8Array[index++] = (word >> 8) & 0xff;
            uInt8Array[index++] = word & 0xff;
        }
        return uInt8Array;
    }

    // Encrypt sym file
    // ****************
    document.getElementById('encryptSymFile').addEventListener('change', function(e) {
        e.preventDefault();
        // remove potential error displayed
        var errorSymFileEncrypt = document.getElementById("errorSymFileEncrypt");
            errorSymFileEncrypt.innerHTML = '';
        
        var key = document.getElementById('symFileKeyLocation').value;
        
        var input = document.getElementById('encryptSymFile');
        
        if('' == key) {                           
            document.getElementById("errorSymFileEncrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.symIndicateYourKey') + '</p>'
            // clear input
            input.value = null;
            return;
        }

        if(key.length < 64) {                           
            document.getElementById("errorSymFileEncrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.symIndicateYourHexKey') + '</p>'
            // clear input
            input.value = null;
            return;
        }  
        encrypt(key, input);
        input.value = null;
        
    })

    // Decrypt sym file
    // ****************
    document.getElementById('decryptSymFile').addEventListener('change', function(e) {
        e.preventDefault();
        // remove potential error displayed
        var errorSymFileDecrypt = document.getElementById("errorSymFileDecrypt");
            errorSymFileDecrypt.innerHTML = '';

        var key = document.getElementById('symFileKeyLocation').value;

        var input = document.getElementById('decryptSymFile');

        if('' == key) {                           
            document.getElementById("errorSymFileDecrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.symIndicateYourKey') + '</p>'
            // clear input
            input.value = null;
            return;
        }    
        
        if(key.length < 64) {                           
            document.getElementById("errorSymFileDecrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.symIndicateYourHexKey') + '</p>'
            // clear input
            input.value = null;
            return;
        }               
        decrypt(key, input);
        input.value = null;    
    })             
})();