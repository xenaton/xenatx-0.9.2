/*------
 CORE
-------*/

/*-----------------
  Lg selector
------------------*/
(function() {

    "use strict";

    document.getElementById('lgSelector')
        .addEventListener('change', function(event) {
        if(this.options[this.selectedIndex].getAttribute('data-country') == 'en') {
          changeLng(event, 'en');
        }
        else {
          changeLng(event, 'fr');
        }
    })
})();

/*-------------------
  File PDF to base64
--------------------*/
(function() {

    "use strict";

    function convertToBase64() {
        //Read File
        var selectedFile = document.getElementById("inputFile").files;
        //Check File is not Empty
        if (selectedFile.length > 0) {
            // Select the very first file from list
            var fileToLoad = selectedFile[0];
            // FileReader function for read the file.
            var fileReader = new FileReader();
            var base64;
            // Onload of file read the file content
            fileReader.onload = function(fileLoadedEvent) {
                base64 = fileLoadedEvent.target.result;
                // Print data in console
                console.log(base64);
            };
            // Convert data to base64
            fileReader.readAsDataURL(fileToLoad);
        }
    }

})();

/*-----------------
 Copy to clipboard
------------------*/
(function($) {

  "use strict";
  // Select elements
  const buttons = document.querySelectorAll('.btn__copy');

  buttons.forEach(button => {
    button.onclick = function() {
      const target = button.previousElementSibling;
      // Init clipboard -- offical documentation: https://clipboardjs.com/
      var clipboard = new ClipboardJS(button, {
          target: target,
          text: function() {
              return target.value;
          }
      });

      // Success action handler
      clipboard.on('success', function(e) {
          const currentLabel = button.innerHTML;

          // Exit label update when already in progress
          if(button.innerHTML === i18next.t('various.copied')){
              return;
          }

          // Update button label
          button.innerHTML = i18next.t('various.copied');

          // Revert button label after 3 seconds
          setTimeout(function(){
              button.innerHTML = currentLabel;
          }, 3000)
      });
    }
  })

})();

/*----------------------
 REMOVE ALL FORM ERRORS
-----------------------*/
(function($) {

  "use strict";
  var ControlForm = function($form){
    this.form = $form;
  };

  ControlForm.prototype.removeError = function() {
    var alertError = document.querySelector("p.alert.alert-danger");

    if(alertError != undefined){
      alertError.remove();
    }
  };

  ControlForm.prototype.init = function() {
    this.inputs = this.form.find('input, textarea');
    this.inputs.on('keypress', {that: this}, this.removeError);
  };

  // init
  $(function() {
    var $form = document.querySelector(".tab-pane");
    if( $form ) {
        var controlForm = new ControlForm( $(this) );
        controlForm.init();
    }
  });

})(window.jQuery);
