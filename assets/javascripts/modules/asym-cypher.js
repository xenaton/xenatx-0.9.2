//*************
// ASYM CYPHER
//*************
(function() {

    "use strict";

    document.getElementById('cypherPrivKeyFile')
        .addEventListener('change', function() {
        
        var fr=new FileReader();
        fr.onload=function(){
            addText(fr.result, "cypherInputPrivateKey");
        }
        fr.readAsText(this.files[0]);
    })

    document.getElementById('cypherPubKeyFile')
        .addEventListener('change', function() {
        
        var fr=new FileReader();
        fr.onload=function(){
            addText(fr.result, "cypherInputPublicKey");
        }
        fr.readAsText(this.files[0]);
    })

    // counter password characters
    var displayEnteredCharAsymPwd = document.getElementById("displayEnteredCharAsymPwd");  
    var variousContent = document.getElementById("asym_create_form_password");   
    const asymPwdCharCounter = () => {
        let numOfEnteredChars = variousContent.value.length;
        displayEnteredCharAsymPwd.textContent = numOfEnteredChars;
    };
    variousContent.addEventListener("input", asymPwdCharCounter);

    //******
    // CRYPT
    //******
    var crypt = document.getElementById('crypt');
    if(crypt){
        crypt.addEventListener('click', function(e){
            e.preventDefault();
            var errorAsymCypherEncrypt = document.getElementById("errorAsymCypherEncrypt");
            errorAsymCypherEncrypt.innerHTML = '';

            var asym_form_password = document.getElementById('asym_form_password').value;
            var cypherInputPrivateKey = document.querySelector('#cypherInputPrivateKey').value;
            var cypherInputPublicKey = document.querySelector('#cypherInputPublicKey').value;
            var messageToCrypt = document.querySelector('#messageToCrypt').value;

            if('' == asym_form_password) {                           
                document.getElementById("errorAsymCypherEncrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.asymPasswordEmpty') + '</p>'
                return;
            }
            
            if('' == cypherInputPrivateKey) {                           
                document.getElementById("errorAsymCypherEncrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.privateKeyEmpty') + '</p>'
                return;
            }

            if('' == cypherInputPublicKey) {                           
                document.getElementById("errorAsymCypherEncrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.publicKeyEmpty') + '</p>'
                return;
            }

            if('' == messageToCrypt) {                           
                document.getElementById("errorAsymCypherEncrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.asymMessageToCryptEmpty') + '</p>'
                return;
            }

            (async () => {
                            
                const publicKeyArmored = cypherInputPublicKey;

                const privateKeyArmored = cypherInputPrivateKey;

                const passphrase = asym_form_password;

                const publicKey = await openpgp.readKey({ armoredKey: publicKeyArmored });

                try {
                    const privateKey = await openpgp.decryptKey({
                        privateKey: await openpgp.readPrivateKey({ armoredKey: privateKeyArmored }),
                        passphrase
                    });
                    const encrypted = await openpgp.encrypt({
                        message: await openpgp.createMessage({ text: messageToCrypt }),
                        encryptionKeys: publicKey,
                        signingKeys: privateKey
                    });
                    addText(encrypted, "encrypted-crypt");

                    setDisabled('crypt');
                    setDisabled('messageToCrypt');
                    removeDisabled('encrypted-crypt');
                    removeDisabled('cancelCrypt');
                
                } 
                catch (e) {
                    document.getElementById("errorAsymCypherEncrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Chiffrement : ' + e.message + '.</p>'
                    return;
                }
                
            })();

        });

        // cancel crypt
        var cancelCrypt = document.getElementById('cancelCrypt');
        if(cancelCrypt){
            cancelCrypt.addEventListener('click', function(e){
                e.preventDefault();
                var errorAsymCypherDecrypt = document.getElementById("errorAsymCypherDecrypt");
                errorAsymCypherDecrypt.innerHTML = '';

                const messageToCrypt = document.getElementById("messageToCrypt");
                    messageToCrypt.value = '';

                const encryptedUncrypt = document.getElementById("encrypted-crypt");
                    encryptedUncrypt.value = '';

                removeDisabled('crypt');
                removeDisabled('messageToCrypt');
                setDisabled('encrypted-crypt');
                setDisabled('cancelCrypt');
            })
        }
    } // / end if crypt

    //********
    // UNCRYPT
    //********
    var uncrypt = document.getElementById('uncrypt');
    if(uncrypt){
        uncrypt.addEventListener('click', function(e){
            e.preventDefault();
            var errorAsymCypherDecrypt = document.getElementById("errorAsymCypherDecrypt");
            errorAsymCypherDecrypt.innerHTML = '';

            var asym_form_password = document.querySelector('#asym_form_password').value;
            var cypherInputPrivateKey = document.querySelector('#cypherInputPrivateKey').value;
            var cypherInputPublicKey = document.querySelector('#cypherInputPublicKey').value;
            var messageToUncrypt = document.querySelector('#messageToUncrypt').value; 

            if('' == asym_form_password) {                           
                document.getElementById("errorAsymCypherDecrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.asymPasswordEmpty') + '</p>'
                return;
            }

            if('' == cypherInputPrivateKey) {                           
                document.getElementById("errorAsymCypherDecrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.privateKeyEmpty') + '</p>'
                return;
            }

            if('' == cypherInputPublicKey) {                           
                document.getElementById("errorAsymCypherDecrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.publicKeyEmpty') + '</p>'
                return;
            }

            if('' == messageToUncrypt) {                           
                document.getElementById("errorAsymCypherDecrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.asymMessageToUncryptEmpty') + '</p>'
                return;
            }
                 

            (async () => {
                            
                const publicKeyArmored = cypherInputPublicKey;
                const privateKeyArmored = cypherInputPrivateKey;
                const passphrase = asym_form_password;

                try {
                    const publicKey = await openpgp.readKey({ armoredKey: publicKeyArmored });
                    const privateKey = await openpgp.decryptKey({
                        privateKey: await openpgp.readPrivateKey({ armoredKey: privateKeyArmored }),
                        passphrase
                    });

                    const message = await openpgp.readMessage({
                        armoredMessage: messageToUncrypt
                    });

                    const { data: decrypted, signatures } = await openpgp.decrypt({
                        message,
                        verificationKeys: publicKey, // optional
                        decryptionKeys: privateKey 
                    });
                    addText(decrypted, "decrypted-uncrypt");

                    setDisabled('uncrypt');
                    setDisabled('messageToUncrypt');
                    removeDisabled('decrypted-uncrypt');
                    removeDisabled('cancelUncrypt');

                    // check signature validity (signed message only)
                    try {
                        await signatures[0].verified; // throws on invalid signature
                        var valid = 'La signature est valide.';
                        addText(valid, "valid-uncrypt");

                    } catch (e) {
                        alert('Signature : ' + e.message);
                    }
                } 
                catch (e) {
                    document.getElementById("errorAsymCypherDecrypt").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> Déchiffrement : ' + e.message + '.</p>'
                    return;
                }

            })();
        });

        // cancel decrypt
        var cancelUncrypt = document.getElementById('cancelUncrypt');
        if(cancelUncrypt){
            cancelUncrypt.addEventListener('click', function(e){
                e.preventDefault();
                var errorAsymCypherDecrypt = document.getElementById("errorAsymCypherDecrypt");
                errorAsymCypherDecrypt.innerHTML = '';

                const messageToUncrypt = document.getElementById("messageToUncrypt");
                    messageToUncrypt.value = '';

                const decryptedUncrypt = document.getElementById("decrypted-uncrypt");
                    decryptedUncrypt.value = '';

                const validUncrypt = document.getElementById("valid-uncrypt");
                    validUncrypt.value = '';

                removeDisabled('uncrypt');
                removeDisabled('messageToUncrypt');
                setDisabled('decrypted-uncrypt');
                setDisabled('cancelUncrypt');
                
            })
        }
    } // / end if uncrypt        
})();
