/*----------------------
 REVOCATION CERTIFICATE
-----------------------*/
(function() {

    var revoke = document.getElementById('btnRevoke');
    if(revoke){
        revoke.addEventListener('click', function(e){
            e.preventDefault();
            var errorRevocation = document.getElementById("errorRevocation");
            errorRevocation.innerHTML = '';

            var revocationPubKeyField = document.getElementById('revocationPubKeyField').value.trim();
            var revocationCertificateField = document.getElementById('revocationCertificateField').value.trim();

            if('' == revocationPubKeyField) {                           
                document.getElementById("errorRevocation").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.revocationPubKeyfieldEmpty') + '</p>'
                return;
            }
            
            if('' == revocationCertificateField) {                           
                document.getElementById("errorRevocation").innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.revocationCertificateFieldEmpty') + '</p>'
                return;
            }

            (async () => {
                const publicKeyArmored = revocationPubKeyField;
                const revocationCertificate = revocationCertificateField;

                (async () => {
                    const { publicKey: revokedKeyArmored } = await openpgp.revokeKey({
                        key: await openpgp.readKey({ armoredKey: publicKeyArmored }),
                        revocationCertificate,
                        format: 'armored' // output armored keys
                    });
                    addText(revokedKeyArmored.trim(), "newPubKeyRevokedField"); // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
                    
                    setDisabled('btnRevoke');
                    setDisabled('revocationPubKeyField');
                    setDisabled('revocationCertificateField');
                    removeDisabled('newPubKeyRevokedField');
                    removeDisabled('cancelRevocation');
                })();

            })();

        });

        // cancel revocation
        var cancelRevocation = document.getElementById('cancelRevocation');
        if(cancelRevocation) {
            cancelRevocation.addEventListener('click', function(e) {
                e.preventDefault();
                var errorRevocation = document.getElementById("errorRevocation");
                errorRevocation.innerHTML = '';

                const revocationPubKeyField = document.getElementById("revocationPubKeyField");
                    revocationPubKeyField.value = '';

                const revocationCertificateField = document.getElementById("revocationCertificateField");
                    revocationCertificateField.value = '';

                const newPubKeyRevokedField = document.getElementById("newPubKeyRevokedField");
                    newPubKeyRevokedField.value = '';

                removeDisabled('btnRevoke');
                removeDisabled('revocationPubKeyField');
                removeDisabled('revocationCertificateField');
                setDisabled('newPubKeyRevokedField');
                setDisabled('cancelRevocation');
            })
        }
    } // end if revocation

})();